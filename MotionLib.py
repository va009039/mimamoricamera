#coding: UTF-8
# MotionLib.py 2016/1/2 モーション検出ライブラリ

import os
from time import *
import datetime
import random
from PIL import Image,ImageTk

class Motion1:
    def __init__(self):
        self.sample = []
        self.old_val = []

    def ginfo(self, buf, xy):
        b = ord(buf[xy])
        g = ord(buf[xy + 1])
        r = ord(buf[xy + 2])
        return (r * 299 + g * 587 + b * 114) / 1000 # 輝度の計算

    def check(self, buf, noize = 30, limit = 12):
        max_sample = 200 # 観測数
        if not self.sample:
            self.sample = random.sample(xrange(0, len(buf)-1, 3), max_sample)
        new_val = []
        for i in range(0, max_sample):
            new_val.append(self.ginfo(buf, self.sample[i]))
        t = 0
        if self.old_val:
            for i in range(0, max_sample):
                if abs(self.old_val[i] - new_val[i]) > noize:
                    t += 1
        self.old_val = new_val
        return (t * 100 / max_sample) > limit

    def getImage(self, buffer, width, height):
        return Image.fromstring('RGB', (width, height), buffer, 'raw', 'BGR', 0, -1)

def getFilename(save_dir):
    dt = datetime.datetime.now()
    dir_name = save_dir + dt.strftime("%Y%m%d/")
    if os.path.isdir(dir_name) == False:
        os.makedirs(dir_name)
    ms = dt.strftime("%f")[0:3] # ミリ秒(ms)
    filename = dir_name + dt.strftime("%H%M%S_") + ms + ".jpg"
    return filename


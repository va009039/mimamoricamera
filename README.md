# みまもりカメラ #

UVCカメラでインターバル撮影、動体検知撮影をします。

### 必要なソフトウェア、モジュール、ライブラリ ###

* Python2.7 
* VideoCapture VideoCapture-0.9-5.zip http://videocapture.sourceforge.net/
* PIL http://www.pythonware.com/products/pil/
* HSPDSC.DLL http://hsp.moe/

### 使い方 ###

デバイス番号0のカメラで撮影、 c:\micam0フォルダに画像ファイルを保存。
python micam.py 0 


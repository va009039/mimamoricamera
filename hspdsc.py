# -*- coding: utf-8 -*-
# hspdsc.py 2011.1.13
import ctypes

class hspdsc:
    def __init__(self):
        # hspdsc.dllの読み込み
        self.dll = ctypes.cdll.LoadLibrary('hspdsc')
        self.dll.dsc_Init() # 初期化

    def Version(self):
        return self.dll.dsc_Version() # バージョンを取得 202

    def GetDeviceList(self):
        # 利用可能なデバイスの一覧を取得
        p1 = ctypes.create_string_buffer(4096)
        self.dll.dsc_GetDeviceList(p1, 0) # 第2引数に0を設定する
        cam = p1.value.decode('shift_jis').splitlines()
        return cam


if __name__ == '__main__':
    dsc = hspdsc()
    print 'version is', dsc.Version()
    cam = dsc.GetDeviceList()
    n = 0
    for s in cam:
        print '%d %s' % (n, s)
        n += 1

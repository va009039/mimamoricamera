# micam.py 2016/1/2
# coding: utf-8
#
# モーション検出＆インターバル撮影

import logging
import sys
import time
import VideoCapture
import MotionLib

class micam:
    def __init__(self, devnum, width=None, height=None, view=False):
        self.devnum = devnum
        self.width = width
        self.height = height
        self.view = 0
        if view:
            self.view = 1
        self.save_dir ="C:/micam%d/" % self.devnum
        self.save_interval = 300
        self.cam = VideoCapture.Device(devnum=self.devnum, showVideoWindow=self.view)
        if self.width and self.height:
            self.cam.setResolution(self.width, self.height)
        self.cam_name = self.cam.getDisplayName()
        logging.info("cam name: [%s]", self.cam_name)
        self.motion = MotionLib.Motion1()
        self.last_snap_time = 0

    def poll(self, wait_sec=0.3):
        while 1:
            self.run()
            time.sleep(wait_sec)

    def run(self):
        now_t = time.time()
        (buf, x, y) = self.cam.getBuffer()
        if self.motion.check(buf): # モーションチェック
            self.save_image(motion=True)
        elif (now_t - self.last_snap_time) > self.save_interval:
            self.save_image()

    def save_image(self, motion=False):
        filename = MotionLib.getFilename(self.save_dir)
        if motion:
            logging.info(filename + " !") # motion
        else:
            logging.info(filename + " +") # interval
        self.cam.saveSnapshot(filename, timestamp=1)
        self.last_snap_time = time.time()

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('devnum', type=int, nargs=1)
    parser.add_argument('--width', type=int, default=None)
    parser.add_argument('--height', type=int, default=None)
    parser.add_argument('--view', action='store_true')
    args = parser.parse_args()
    devnum = args.devnum[0]
    logging.info("devnum=%d", devnum)
    if args.width and args.height:
        logging.info("width=%d, height=%d", args.width, args.height)
    logging.info("view=%d", args.view)
    cam = micam(devnum, args.width, args.height, args.view)
    cam.poll()

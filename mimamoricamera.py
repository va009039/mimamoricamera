# mimamoricamera.py 2015/12/25
# coding: utf-8
#
# モーション検出＆インターバル撮影

import logging
import sys
import ConfigParser
from optparse import OptionParser
import time
import threading
from Tkinter import *
import VideoCapture
import PIL
import MotionLib

parser = ConfigParser.ConfigParser()
parser.read('mimamoricamera.ini')
cfg = {
    'devnum': parser.getint('config', 'devnum'),
    'save_dir': parser.get('config', 'save_dir'),
    'save_interval': parser.getint('config', 'save_interval'),
}

parser = OptionParser()
parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False)
parser.add_option("-l", "--list", action="store_true", dest="devlist", default=False,
                    help=u"デバイスリストの表示")
parser.add_option("-d", "--devnum", type="int", dest="devnum", default=cfg['devnum'],
                    help=u"カメラのデバイス番号")
parser.add_option("-s", "--save_dir", dest="save_dir", default=cfg['save_dir'],
                    help=u"画像を保存するディレクトリ")
(options, args) = parser.parse_args()

if options.devlist:
    import hspdsc
    dsc = hspdsc.hspdsc()
    devlists = dsc.GetDeviceList()
    print u'デバイスリストの表示'
    n = 0
    for s in devlists:
        print '%d %s' % (n, s)
        n += 1
    sys.exit()

if options.verbose:
    print 'options is:'
    print options

class App(Frame):
    def __init__(self, master = None):
        Frame.__init__(self, master)
        self.pack()
        self.init()
        self.event_timer()

    def init(self):
        self.cam = VideoCapture.Device(devnum=options.devnum, showVideoWindow=0)
        self.cam_name = self.cam.getDisplayName()
        logging.info("cam name: [%s]", self.cam_name)
        self.master.title(u'みまもりカメラ(%d:%s)' % (options.devnum, self.cam_name))
        self.master.option_add('*font', ('FixedSys', 14))
        self.cam_lock = threading.Lock()
        #self.cam.setResolution(width=320, height=240)
        self.image_data = PIL.ImageTk.PhotoImage(self.cam.getImage())
        self.label2 = Label(self, image = self.image_data)
        self.label2.pack()
        self.motion = MotionLib.Motion1()
        self.last_snap_time = 0

    def event_timer(self):
        now_t = time.time()
        self.cam_lock.acquire()
        (buf, x, y) = self.cam.getBuffer()
        self.cam_lock.release()
        if self.motion.check(buf): # モーションチェック
            self.command_save_image(motion=True)
        elif (now_t - self.last_snap_time) > cfg['save_interval']:
            self.command_save_image()
        else:
            self.update_image()
        self.master.after(500, self.event_timer)

    def update_image(self):
        self.cam_lock.acquire()
        self.image_data = PIL.ImageTk.PhotoImage(self.cam.getImage())
        self.cam_lock.release()
        self.label2.configure(image = self.image_data)

    def command_save_image(self, motion=False):
        filename = MotionLib.getFilename(options.save_dir)
        if options.verbose:
            if motion:
                logging.info(filename + " !") # motion
            else:
                logging.info(filename + " +") # interval
        self.cam_lock.acquire()
        self.cam.saveSnapshot(filename)
        self.cam_lock.release()
        self.last_snap_time = time.time()

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    app = App()
    app.mainloop()
